<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Lot;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\LotRequest;
use App\Http\Resources\LotResource;
use App\Http\Controllers\Controller;
use App\Interfaces\LotRepositoryInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class LotController extends Controller
{
    private LotRepositoryInterface $lotRepository;

    public function __construct(LotRepositoryInterface $lotRepository)
    {
        $this->lotRepository = $lotRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): AnonymousResourceCollection
    {
        return LotResource::collection($this->lotRepository->getAll());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LotRequest $request)
    {
        $request->validate([
            'name' => 'required|alpha_num',
            'group_id' => 'required|exists:App\Models\Group,id'
        ]);

        $lot = $request->only([
            'name',
            'group_id',
        ]);

        return (new LotResource($this->lotRepository->create($lot)))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\lot  $lot
     * @return \Illuminate\Http\Response
     */
    public function show(lot $lot)
    {
        return LotResource::make($lot);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\lot  $lot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, lot $lot)
    {
        $request->validate([
            'name' => 'required|alpha_num',
            'group_id' => 'required|exists:App\Models\Group,id'
        ]);

        $update = $request->only([
            'name',
            'group_id',
        ]);

        $lotId = $lot->id;

        $this->lotRepository->update($lotId, $update);

        return (new LotResource($this->lotRepository->getById($lotId)))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\lot  $lot
     * @return \Illuminate\Http\Response
     */
    public function destroy(lot $lot)
    {
        $lotId = $lot->id;
        $this->lotRepository->delete($lotId);

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
