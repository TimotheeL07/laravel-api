<?php

namespace App\Repositories;

use App\Interfaces\LotRepositoryInterface;
use App\Models\Lot;

class LotRepository implements LotRepositoryInterface
{
    protected $model;

    public function __construct(Lot $lot)
    {
        $this->model = $lot;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getById(int $lotId)
    {
        return $this->model->findOrFail($lotId);
    }

    public function delete(int $lotId)
    {
        $this->model->destroy($lotId);
    }

    public function create(array $lot)
    {
        return $this->model->create($lot);
    }

    public function update(int $lotId, array $lot)
    {
        return $this->model->whereId($lotId)->update($lot);
    }
}
