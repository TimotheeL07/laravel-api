<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            [
                'id' => 1,
                'name' => 'Client A',
                'parent_group_id' => null,
                'group_type_id' => 1,
            ],
            [
                'id' => 2,
                'name' => 'Client B',
                'parent_group_id' => null,
                'group_type_id' => 1,
            ],
            [
                'id' => 3,
                'name' => 'Région A1',
                'parent_group_id' => 1,
                'group_type_id' => 2,
            ],
            [
                'id' => 4,
                'name' => 'Région A2',
                'parent_group_id' => 1,
                'group_type_id' => 2,
            ],
            [
                'id' => 5,
                'name' => 'Région B1',
                'parent_group_id' => 2,
                'group_type_id' => 2,
            ],
            [
                'id' => 6,
                'name' => 'Programme A2_001',
                'parent_group_id' => 4,
                'group_type_id' => 3,
            ],
            [
                'id' => 7,
                'name' => 'Programme A2_002',
                'parent_group_id' => 4,
                'group_type_id' => 3,
            ],
            [
                'id' => 8,
                'name' => 'Programme B1_001',
                'parent_group_id' => 5,
                'group_type_id' => 3,
            ]
        ]);
    }
}
