<?php

namespace App\Providers;

use App\Repositories\LotRepository;
use App\Repositories\GroupRepository;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\LotRepositoryInterface;
use App\Interfaces\GroupRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(GroupRepositoryInterface::class, GroupRepository::class);
        $this->app->bind(LotRepositoryInterface::class, LotRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
