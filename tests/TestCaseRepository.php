<?php

namespace Tests;

use Mockery;
use PHPUnit\Framework\TestCase;

class TestCaseRepository extends TestCase
{
    public function tearDown(): void
    {
        Mockery::close();
    }

    public function mock($class)
    {
        $this->mock = Mockery::mock($class);

        $this->app->instance($class, $this->mock);

        return $this->mock;
    }
}


