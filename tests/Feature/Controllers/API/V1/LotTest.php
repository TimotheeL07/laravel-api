<?php

namespace Tests\Feature\Controllers\API\V1;

use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LotTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('/api/v1/lots');

        $response->assertOk();
    }

    /**
     * @return void
     */
    public function test_show()
    {
        $response = $this->get('/api/v1/lots/1');

        $response->assertOk();

        // ERROR CASES

        $response = $this->get('/api/v1/lots/1000');

        $response->assertNotFound();

        $response = $this->get('/api/v1/lots/test');

        $response->assertNotFound();
    }

    /**
     * @return void
     */
    public function test_store()
    {
        $response = $this->postJson('/api/v1/lots', [
            'name' => '401',
            'group_id' => 6,
            'error' => 3,
        ]);

        $response->assertStatus(Response::HTTP_CREATED);

        // ERROR CASES

        $response = $this->postJson('/api/v1/lots', [
            'name' => 'F104',
            'group_id' => 42,
        ]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);

        $response = $this->postJson('/api/v1/lots', [
            'name' => 'F104',
        ]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return void
     */
    public function test_update()
    {
        $response = $this->postJson('/api/v1/lots', [
            'name' => 'F104',
            'group_id' => 7,
        ]);

        $response->assertStatus(Response::HTTP_CREATED);

        // ERROR CASES

        $response = $this->postJson('/api/v1/lots', [
            'name' => 'F104',
            'group_id' => 42,
        ]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return void
     */
    public function test_destroy()
    {
        $response = $this->delete('/api/v1/lots/1');

        $response->assertNoContent();

        // ERROR CASES

        $response = $this->delete('/api/v1/lots/1000');

        $response->assertNotFound();
    }
}
