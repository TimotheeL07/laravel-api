<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('group_types')->insert([
            [
                'id' => 1,
                'label' => 'Client'
            ],
            [
                'id' => 2,
                'label' => 'Région'
            ],
            [
                'id' => 3,
                'label' => 'Programme'
            ]
        ]);
    }
}
