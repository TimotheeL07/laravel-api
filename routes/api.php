<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return [
        'success' => true,
        'message' => 'Welcome to Test API',
        'data' => []
    ];
});

Route::apiResource('groups', V1\GroupController::class);
Route::apiResource('lots', V1\LotController::class);