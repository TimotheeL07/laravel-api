<?php

namespace Tests\Feature\Controllers\API\V1;

use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GroupTest extends TestCase
{
    use RefreshDatabase;
    
    /**
     * @return void
     */
    public function test_index()
    {
        $response = $this->get('/api/v1/groups');

        $response->assertOk();
    }

    /**
     * @return void
     */
    public function test_show()
    {
        $response = $this->get('/api/v1/groups/1');

        $response->assertOk();

        // ERROR CASES

        $response = $this->get('/api/v1/groups/1000');

        $response->assertNotFound();

        $response = $this->get('/api/v1/groups/test');

        $response->assertNotFound();
    }

    /**
     * @return void
     */
    public function test_store()
    {
        $response = $this->postJson('/api/v1/groups', [
            'name' => 'Programme B1_006',
            'parent_group_id' => 5,
            'group_type_id' => 3,
        ]);

        $response->assertStatus(Response::HTTP_CREATED);

        $response = $this->postJson('/api/v1/groups', [
            'name' => 'Programme B1_006',
            'group_type_id' => 3,
        ]);

        $response->assertStatus(Response::HTTP_CREATED);

        // ERROR CASES

        $response = $this->postJson('/api/v1/lots', [
            'name' => 'F104',
            'parent_group_id' => 50,
            'group_type_id' => 3,
        ]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);

        $response = $this->postJson('/api/v1/lots', [
            'name' => 'F104',
            'parent_group_id' => 5,
        ]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    /**
     * @return void
     */
    public function test_update()
    {
        $response = $this->postJson('/api/v1/groups', [
            'name' => 'Programme B1_008',
            'parent_group_id' => 5,
            'group_type_id' => 3,
        ]);

        $response->assertStatus(Response::HTTP_CREATED);
    }

    /**
     * @return void
     */
    public function test_destroy()
    {
        $response = $this->delete('/api/v1/groups/1');

        $response->assertNoContent();

        // ERROR CASES

        $response = $this->delete('/api/v1/groups/1000');

        $response->assertNotFound();
    }
}
