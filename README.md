# Laravel API

Une application Laravel API qui échange avec une base de données et traite
les résultats en les envoyant en JSON.

## INSTALLATION

Installation de l'application laravel-api depuis github. L'application a besoin d'une version PHP supérieur à PHP 7.3 et d'une base de donnée mysql avec le nom laravel.

```bash
  git clone git@gitlab.com:TimotheeL07/laravel-api.git
  cd laravel-api
  cp .env.example .env
  composer install
  php artisan migrate --seed
  php artisan serve
```

## API Reference


#### Get all groups

```http
  GET /api/v1/groups
```

#### Get group

```http
  GET /api/v1/groups/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of group to fetch |

#### POST group

```http
  POST /api/v1/groups/
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `name`      | `string` | **Required**. Name of group |
| `parent_group_id`      | `integer` or `null` | Id of group |
| `group_type_id`        | `integer` | **Required**. Id of group type |

#### PUT group

```http
  PUT /api/v1/groups/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of group to fetch |

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `name`      | `string` | **Required**. Name of group |
| `parent_group_id`      | `integer` or `null` | Id of group |
| `group_type_id`      | `integer` | **Required**. Id of group type |

#### DELETE group

```http
  DELETE /api/v1/groups/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of group to fetch |

#### Get all lots

```http
  GET /api/v1/lots
```

#### Get lot

```http
  GET /api/v1/lots/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of lot to fetch |

#### POST lot

```http
  POST /api/v1/lots/
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `name`      | `string` | **Required**. Name of lot |
| `group_id`  | `integer`|  **Required**. Id of lot |

#### PUT lot

```http
  PUT /api/v1/lots/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of lot |

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `name`      | `string` | **Required**. Name of lot |
| `group_id`  | `integer`|  **Required**. Id of lot |

#### DELETE lot

```http
  DELETE /api/v1/lots/${id}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `id`      | `string` | **Required**. Id of lot |

## Exécution des tests

Pour exécuter les tests, exécutez la commande suivante

```bash
  php artisan test
```
