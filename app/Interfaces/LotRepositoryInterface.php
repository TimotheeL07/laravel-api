<?php

namespace App\Interfaces;

interface LotRepositoryInterface
{
    public function getAll();
    public function getById(int $lotId);
    public function delete(int $lotId);
    public function create(array $lot);
    public function update(int $lotId, array $data);
}
