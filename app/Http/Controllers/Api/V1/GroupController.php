<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\GroupRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\GroupResource;
use App\Interfaces\GroupRepositoryInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class GroupController extends Controller
{
    private GroupRepositoryInterface $groupRepository;

    public function __construct(GroupRepositoryInterface $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): AnonymousResourceCollection
    {
        return GroupResource::collection($this->groupRepository->getAll());
    }

    /**
     * @param GroupRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupRequest $request)
    {
        $group = $request->only([
            'name',
            'parent_group_id',
            'group_type_id',
        ]);

        return (new GroupResource($this->groupRepository->create($group)))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        return GroupResource::make($group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param GroupRequest $request
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(GroupRequest $request, Group $group)
    {
        $update = $request->only([
            'name',
            'parent_group_id',
            'group_type_id',
        ]);

        $groupId = $group->id;

        $this->groupRepository->update($groupId, $update);

        return (new GroupResource($this->groupRepository->getById($groupId)))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function destroy(Group $group)
    {
        $groupId = $group->id;
        $this->groupRepository->delete($groupId);

        return response()->json(null, Response::HTTP_NO_CONTENT);
    }
}
