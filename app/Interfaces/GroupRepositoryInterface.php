<?php

namespace App\Interfaces;

interface GroupRepositoryInterface
{
    public function getAll();
    public function getById(int $groupId);
    public function delete(int $groupId);
    public function create(array $group);
    public function update(int $groupId, array $data);
}
