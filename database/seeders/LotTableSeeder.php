<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lots')->insert([
            [
                'id' => 1,
                'name' => '101',
                'group_id' => 6,
            ],
            [
                'id' => 2,
                'name' => '102',
                'group_id' => 6,
            ],
            [
                'id' => 3,
                'name' => '201',
                'group_id' => 6,
            ],
            [
                'id' => 4,
                'name' => '202',
                'group_id' => 6,
            ],
            [
                'id' => 5,
                'name' => '2001',
                'group_id' => 6,
            ],
            [
                'id' => 6,
                'name' => 'F102',
                'group_id' => 7,
            ],
            [
                'id' => 7,
                'name' => 'F103',
                'group_id' => 7,
            ],
            [
                'id' => 8,
                'name' => '301',
                'group_id' => 6,
            ]
        ]);
    }
}
