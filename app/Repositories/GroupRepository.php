<?php

namespace App\Repositories;

use App\Interfaces\GroupRepositoryInterface;
use App\Models\Group;

class GroupRepository implements GroupRepositoryInterface
{
    protected $model;

    public function __construct(Group $group)
    {
        $this->model = $group;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getById(int $groupId)
    {
        return $this->model->findOrFail($groupId);
    }

    public function delete(int $groupId)
    {
        $this->model->destroy($groupId);
    }

    public function create(array $group)
    {
        return $this->model->create($group);
    }

    public function update(int $groupId, array $group)
    {
        return $this->model->whereId($groupId)->update($group);
    }
}
